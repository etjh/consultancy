---
title: "Kafka Academy"
date: 2019-05-17T20:45:13+02:00
draft: false
---
The last two days I enjoyed the course for Apache Kafka for Developers by the [kafka.academy](https://superfluid.today) with Herbrand Hofker.
We produced, streamed and consumed on topics in Java. He explained why we should look at data as events and why the current RDB is too slow for the current internet speeds. Kafka will be the truth and the DB will eventually be consistent. Amen.