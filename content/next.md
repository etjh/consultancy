---
title: "Next"
date: 2020-10-02T00:00:00+02:00
draft: false
menu: "main" #Display this page on the nav menu
weight: 40 #Right-most nav item
meta: false #Do not display tags or categories
---
From 1992 on I work as a programmer, developer, designer, Scrum Master or coach on many big and complex projects.
Starting with Clipper, moving to the Oracle DB and now on open source in the Cloud.

At Ahold I helped my team to move from Oracle to Kotlin microservices on Spring and the transformation to DevOps.

In 2019 I also enjoyed the Apache Kafka for Developers Course by the [kafka.academy](https://superfluid.today).
The next challenge is to help you thinking Event with e.g. Event Storming, CQRS and DDD.

Next I would like to join a Kotlin project.
Great ideas are always welcome. Please write me a note on next@etjh.nl.

See you next!