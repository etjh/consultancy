---
title: "About"
date: 2019-05-09T02:27:33+02:00
draft: false
menu: "main" #Display this page on the nav menu
weight: 30 #Right-most nav item
meta: false #Do not display tags or categories
---

> etjh.nl is created with [Hugo](https://gohugo.io/), [Terminal](https://themes.gohugo.io/hugo-theme-terminal/), [VS Code](https://code.visualstudio.com/), [GitLab](https://about.gitlab.com/) and [favicon.io](https://favicon.io/favicon-generator/)

Thanks to Spencer Lyon, Radek Kozieł and John Sorrentin